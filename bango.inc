<?php
function bango_footer(){
  //********** Bango Page tracking code - PHP ********** 
  
  //Extract the current URL from the request, removing any query string variables
  $pageURL = "http://";
  
  //Determine protocol (SSL)
  $protocol = "http://";
  if(getenv("HTTPS") == "on")
  {
   $pageURL = "https://";
   $protocol = "https://";
  }
  
  $pageURL .= getenv("SERVER_NAME") . getenv("SCRIPT_NAME");
  
  //Make sure the page URL is URL Encoded!
  $pageURL = rawurlencode($_GET['q']);
  
  //Specify the title of the current page
  $pageTitle = drupal_get_title();
  
  //Ensure pageTitle is URL Encoding safe
  $pageTitle = rawurlencode($pageTitle);
  
  if(strlen($pageTitle) > 0)
  {
   $pageTitle = "&amp;title=" . $pageTitle;
  }
  
  //Extract the referrer from the environment server variables
  $referrer = getenv("HTTP_REFERER");
  $ba_referrer = "";
  
  //Check to ensure there is a value in the referrer
  if(strlen($referrer) > 0)
  {
   //Add the referrer to the ba_referrer variable.
   //Make sure the referrer is URL Encoded!
   $ba_referrer = "&amp;referrer=" . rawurlencode($referrer);
  }
  
  //Extract the querystring if available
  $querystring = drupal_query_string_encode($_GET, array('q'));  
  //Check to ensure there is a value in the querystring
  if(strlen($querystring) > 0)
  {
   $querystring = "&amp;" . $querystring;
  }  
  return theme('bango_image', variable_get('bango_id'), $pageURL, $pageTitle, $ba_referrer, $querystring);
}

function theme_bango_image($id, $pageURL = '', $pageTitle = '', $ba_referrer = '', $querystring = '', $protocol = "http://") {
  $output = '<div>';
  $output .= theme($protocol . 'bango.net/id/' . $id . '.gif?url=' . $pageURL . $pageTitle . $ba_referrer . $querystring, '', '', array("height" => 1, "width" => 1));
}
