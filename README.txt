INTRODUCTION
============
Bango analytics helps you in creating detailed analytics of users on your mobile site.

See http://bango.com/mobileanalytics/ for more details.

USAGE & INSTALLATION
=====================
The Bango module requires you to have a Bango ID before tracking can begin.

The module will add following tracking parameters:
- title 
- url
- referrer

Any other parameters passed to your webpage (campaign parameters) will be tracked too by Bango.
Campaign parameters will propagate through the different pages during a visit.

CACHING
=======
When caching is enabled, Bango does not allow tracking of the dynamic parameters. This is mainly everything except of the page title and the url.


AUTHOR
======
Contact me for help of more info on this module: http://drupal.org/user/25564 (twom)
